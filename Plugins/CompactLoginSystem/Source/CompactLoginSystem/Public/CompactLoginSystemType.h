// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#include "Json.h"
#include "JsonUtilities.h"
#include "CompactLoginSystemType.generated.h"

//explore to blueprint
USTRUCT(BlueprintType)
struct FCustomizedJson
{
	GENERATED_BODY()

public:
	// native json value
	TSharedPtr<FJsonValue> NativeJsonValue;

	// native json key
	UPROPERTY()
	FString NativeJsonKey;

	// is valid json
	UPROPERTY(BlueprintReadWrite)
	bool bIsValid = true;
};