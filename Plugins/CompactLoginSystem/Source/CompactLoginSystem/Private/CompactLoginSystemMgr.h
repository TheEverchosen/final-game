// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CompactLoginSystemType.h"
#include "CompactLoginSystemMgr.generated.h"

DECLARE_DYNAMIC_DELEGATE_TwoParams(FOnServerResponseDelegate, const FCustomizedJson&, ParsedJson, const FString&, JsonString);

UCLASS(BlueprintType)
class ACompactLoginSystemMgr : public AActor
{
	GENERATED_UCLASS_BODY()

public:
	static TWeakObjectPtr<ACompactLoginSystemMgr> LoginMgrRef;

public:

	virtual void BeginPlay() override;

	UFUNCTION(BlueprintPure, meta = (HidePin = "WorldContentObject", DefaultToSelf = "WorldContentObject"))
	static ACompactLoginSystemMgr* GetLoginSystemManager(UObject* WorldContentObject);

	//Customized request
	UFUNCTION(BlueprintCallable)
	void SendClientRequest(FString ServerAddress, TArray<FCustomizedJson> RequestJsonArray, const FOnServerResponseDelegate& ResponseCallback);

	void WebServerResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

private:

	TMap<FHttpRequestPtr, FOnServerResponseDelegate> ResponseDelegateMap;

};

