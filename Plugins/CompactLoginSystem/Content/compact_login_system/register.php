<?php

  // Get the conent
  $post_data = file_get_contents('php://input');

  // decode json
  $obj = json_decode($post_data);

  //email
  $email = $obj->{'email'};

  //user name
  $username = $obj->{'username'};

  //password
  $password = $obj->{'password'};

  //nick name
  //$nickname = $obj->{'nickname'};

  // authcode
  $authcode = $obj->{'authcode'};
  
  // connects to dabtabase
  $db = new PDO("sqlite:" . "databases/UserData.db");
  
  // check exist
  $userfound = FALSE;    
  $query = "SELECT username FROM users";
  foreach($db->query($query) as $data) 
  {
    if($username == $data['username']) 
    {
        $userfound = TRUE;
        break;
    }
  }

  //Get authcode
  $authcodefound = FALSE;
  $authcode_timestamp = 0;
  $authcodeintalbe = '';
  $query = "SELECT * FROM auth WHERE email = '$email'";
  foreach($db->query($query) as $data) 
  {
      $authcodeintalbe = $data['authcode'];
      if($authcode == $authcodeintalbe)
      {
        $authcodefound = TRUE;
      }
      $authcode_timestamp = $data['timestamp'];
  }

  //check can register
  $retcode = 0;
  $error = 'Register successful!';
  if(!$userfound)
  {     
    if(!$authcodefound)
    {
      $retcode = 2;
      $error = 'Authority code error, please regenerate! ' . $authcode . ': ' . $authcodeintalbe . '; time' . $authcode_timestamp;
    }
    else
    {
      list($msec, $sec) = explode(' ', microtime());
      $current_timestamp = $sec;
      $timedis = $current_timestamp - $authcode_timestamp;
      //ten min effective.
      if($timedis > 600)
      {
        $retcode = 3;
        $error = 'Authority code time out, please regenerate!';
      }
    }
  }
  else
  {
    $retcode = 1;
    $error = 'User is already existing, please login!';
  }

  //register
  $ret = TRUE;
  if($retcode == 0)
  {
    $count = 0;
    $query = "SELECT username FROM users";
    foreach($db->query($query) as $data) {
        $count = $count + 1;
    }
    $count = $count + 1;
    $password64 = base64_encode($password);
    $sql = "INSERT INTO users (id,username,password,name,email,phone,role,active,last) VALUES ($count, '$username', '$password64', '--',  '$email','+', 'guest', 'No', 'Never' );";
    $ret = $db->exec($sql);
  }

  if ($ret == FALSE)
  {
    $retcode = 4;
    $error = 'Register fail, check connection!';
  }

  //result
  require('util.php');
  $rep = new ClientReqResp();
  $rep->retcode = $retcode;
  $rep->message = $error;

  echo json_encode($rep);
?>